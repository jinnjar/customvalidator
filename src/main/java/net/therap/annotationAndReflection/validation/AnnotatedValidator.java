package net.therap.annotationAndReflection.validation;

import java.lang.reflect.Field;
import java.text.Annotation;
import java.text.MessageFormat;
import java.util.List;

/**
 * @author mohammad.hossain
 * @since 12/26/21
 */
public class AnnotatedValidator {

    public static <T> void validate(T t, List<ValidationError> errorList) {
        Field[] fields = t.getClass().getDeclaredFields();

        for (Field field : fields) {
            field.setAccessible(true);

            if (field.isAnnotationPresent(Size.class)) {
                Size size = field.getAnnotation(Size.class);

                try {
                    if (field.getType().equals(String.class)) {
                        String val = (String) field.get(t);

                        if (val.length() < size.min() || val.length() > size.max()) {
                            String message = processMessage(size);
                            errorList.add(new ValidationError(field, message));
                        }
                    } else if (field.getType().equals(int.class)) {
                        int val = (int) field.get(t);

                        if (val < size.min() || val > size.max()) {
                            String message = processMessage(size);
                            errorList.add(new ValidationError(field, message));
                        }
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void print(List<ValidationError> errorList) {
        for (ValidationError e : errorList) {
            System.out.printf("%s(%s): %s%n",
                    e.getField().getName(),
                    e.getField().getType().getSimpleName(),
                    e.getMessage());
        }
    }

    private static String processMessage(Size size) {
        String message = size.message()
                .replaceAll("min", "0")
                .replaceAll("max", "1");
        return MessageFormat.format(message, size.min(), size.max());
    }
}