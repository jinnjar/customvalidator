package net.therap.annotationAndReflection.validation;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.lang.reflect.Field;

/**
 * @author mohammad.hossain
 * @since 12/26/21
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ValidationError {

    private Field field;
    private String message;
}