package net.therap.annotationAndReflection.validation;


import java.lang.annotation.*;

/**
 * @author mohammad.hossain
 * @since 12/26/21
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Size {

    int max() default 100;

    int min() default 1;

    String message() default "Length must be {min}-{max}";
}