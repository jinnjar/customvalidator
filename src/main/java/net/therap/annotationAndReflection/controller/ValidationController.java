package net.therap.annotationAndReflection.controller;

import net.therap.annotationAndReflection.model.Person;
import net.therap.annotationAndReflection.validation.ValidationError;
import net.therap.annotationAndReflection.validation.AnnotatedValidator;

import java.util.ArrayList;
import java.util.List;

/**
 * @author mohammad.hossain
 * @since 12/26/21
 */
public class ValidationController {

    public static void init() {
        Person p = new Person("Adnan Hossain", 17);
        List<ValidationError> errors = new ArrayList<>();
        AnnotatedValidator.validate(p, errors);
        AnnotatedValidator.print(errors);
    }
}