package net.therap.annotationAndReflection;

import net.therap.annotationAndReflection.controller.ValidationController;

/**
 * @author mohammad.hossain
 * @since 12/26/21
 */
public class Main {

    public static void main(String[] args) {
        ValidationController.init();
    }
}