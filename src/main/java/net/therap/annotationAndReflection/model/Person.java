package net.therap.annotationAndReflection.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import net.therap.annotationAndReflection.validation.Size;

/**
 * @author mohammad.hossain
 * @since 12/26/21
 */
@Data
@AllArgsConstructor
public class Person {

    @Size(max = 10)
    private String name;

    @Size(min = 18, message = "Age can not be less than {min}")
    private int age;
}